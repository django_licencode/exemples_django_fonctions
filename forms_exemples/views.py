from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Personne
from .forms import FormPersonne


def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = FormPersonne(request.POST)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            # redirect to a new URL:
            #return HttpResponseRedirect("/thanks/")

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FormPersonne()

    return render(request, "forms_exemples/name.html", {"form": form})
