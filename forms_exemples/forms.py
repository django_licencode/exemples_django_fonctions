from django import forms

from .models import Personne

class FormPersonne(forms.ModelForm):
    class Meta:
        model = Personne
        fields = ["fullname", "mobile_number",]
        labels = {"fullname": "Name", "mobile_number": "Mobile Number",}
